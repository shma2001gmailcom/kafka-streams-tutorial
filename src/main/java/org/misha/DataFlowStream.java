package org.misha;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;

import java.util.Properties;

import static org.apache.kafka.streams.StreamsConfig.*;

public class DataFlowStream {
    public static void main(String[] args) {
        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> stream = builder.stream("streams-dataflow-input");
        stream.foreach((key, value) -> System.out.printf("Key %s; Value %s", key, value));
        stream.filter((key, value) -> value.contains("token"))
                .map((key, value) -> KeyValue.pair(key, value.toUpperCase())).to("streams-dataflow-output");
        Topology topology = builder.build();
        System.out.println(topology.describe());
        try (KafkaStreams streams = new KafkaStreams(topology, makeProperties())) {
            streams.start();
            Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
        }
    }

    private static Properties makeProperties() {
        Properties props = new Properties();
        props.put(APPLICATION_ID_CONFIG, "streams-dataflow");
        props.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        return props;
    }
}
