package org.misha;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Produced;

import java.util.Arrays;
import java.util.Properties;

import static org.apache.kafka.streams.StreamsConfig.*;

public class WordCountStream {

    public static void main(String[] args) {
        makeProperties();
        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> stream = builder.stream("streams-wordcount-input");
        KGroupedStream<String, String> groupedStream = stream.flatMapValues(value -> Arrays.asList(value.toLowerCase().split(" ")))
                .groupBy(((key, value) -> value));
        KTable<String, Long> counts = groupedStream.count();
        counts.toStream().to("streams-wordcount-output", Produced.with(Serdes.String(), Serdes.Long()));
        Topology topology = builder.build();
        System.out.println(topology.describe());
        try (KafkaStreams streams = new KafkaStreams(topology, makeProperties())) {
            streams.start();
            Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
        }
    }

    private static Properties makeProperties() {
        Properties props = new Properties();
        props.put(APPLICATION_ID_CONFIG, "streams-wordcount");
        props.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        return props;
    }
}
